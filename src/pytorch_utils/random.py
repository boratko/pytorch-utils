# Copyright 2023 The pytorch-utils Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from typing import *

import torch
from torch import LongTensor

from . import TorchShape

__all__ = [
    "RandomIntsAvoid",
]


class RandomIntsAvoid:
    """
    Return uniformly randomly sampled positive integers while avoiding some predetermined list of ints.

    When called, this returns a LongTensor of integers in
        {i : i in N, i not in avoid}
    sampled uniformly randomly. To allow API consistency, you can call this without specifying `avoid`, in which case
    it works just like `torch.randint`.

    :param max: sampled integers will be < max
    :param avoid: LongTensor of ints to avoid sampling
    :param sorted: set True if avoid is already sorted to avoid unnecessary computation
    :param unique: set True if avoid is already unique to avoid unnecessary computation
    """

    def __init__(
        self,
        max: int,
        avoid: Optional[LongTensor] = None,
        sorted: bool = False,
        unique: bool = False,
        device: Union[None, str, torch.device] = None,
    ):
        if avoid is None:
            self._device = device or torch.device("cpu")
            self._sample_max = max
            self._buckets = None
        else:
            self._device = device or avoid.device
            self._sample_max = max - len(avoid)
            if not sorted:
                avoid = torch.sort(avoid)[0]
            if not unique:
                avoid = torch.unique_consecutive(avoid)
            self._buckets = avoid - torch.arange(len(avoid), device=device)

    def __call__(self, size: TorchShape):
        if isinstance(size, int):
            size = (size,)
        sample_idxs = torch.randint(
            high=self._sample_max, size=size, device=self._device
        )
        if self._buckets is None:
            return sample_idxs
        else:
            return sample_idxs + torch.bucketize(sample_idxs, self._buckets, right=True)

    def to(self, device: Union[str, torch.device]):
        self.device = device
        if self._buckets is not None:
            self._buckets = self._buckets.to(device)
        return self
