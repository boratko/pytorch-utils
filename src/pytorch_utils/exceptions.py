# Copyright 2023 The pytorch-utils Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import attr

__all__ = [
    "LooperException",
    "StopLoopingException",
    "EarlyStoppingException",
]


class LooperException(Exception):
    """Base class for exceptions encountered during looping"""

    pass


class StopLoopingException(LooperException):
    """Base class for exceptions which should stop training in this module."""

    pass


@attr.s(auto_attribs=True)
class EarlyStoppingException(StopLoopingException):
    """Max Value Exceeded"""

    condition: str

    def __str__(self):
        return f"EarlyStopping: {self.condition}"
