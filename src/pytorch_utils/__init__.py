# Copyright 2023 The pytorch-utils Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import gc
from typing import *

import attr
import torch
from loguru import logger
from torch import Tensor

from .tensordataloader import TensorDataLoader, TensorBatchSampler

__all__ = [
    "TorchShape",
    "cuda_if_available",
    "is_tensor",
    "all_tensors",
    "is_broadcastable",
    "mem_stats",
    "TruncateIter",
    "unique",
    "TensorDataLoader",
    "TensorBatchSampler",
]

TorchShape = Union[int, torch.Size, List[int], Tuple[int, ...]]


def cuda_if_available(use_cuda: Optional[bool] = None) -> torch.device:
    cuda_available = torch.cuda.is_available()
    _use_cuda = (use_cuda is None or use_cuda) and cuda_available
    if use_cuda is True and not cuda_available:
        logger.warning("Requested CUDA but it is not available, running on CPU")
    if use_cuda is False and cuda_available:
        logger.warning(
            "Running on CPU, even though CUDA is available. "
            "(This is likely not desired, check your arguments.)"
        )
    return torch.device("cuda" if _use_cuda else "cpu")


def is_tensor(obj: Any) -> bool:
    """ Return True if `obj` is a tensor, False otherwise """
    try:
        if torch.is_tensor(obj) or (hasattr(obj, "data") and torch.is_tensor(obj.data)):
            return True
        else:
            return False
    except:
        # Some objects throw an error when running hasattr
        return False


def all_tensors(device: Optional[Union[torch.device, str]] = None) -> List[Tensor]:
    if device is None:
        return [obj for obj in gc.get_objects() if is_tensor(obj)]
    else:
        return [
            obj for obj in gc.get_objects() if is_tensor(obj) and obj.device == device
        ]


def is_broadcastable(A: TorchShape, B: TorchShape):
    if isinstance(A, int):
        A = [A]
    if isinstance(B, int):
        B = [B]
    return all((m == n) or (m == 1) or (n == 1) for m, n in zip(A[::-1], B[::-1]))


def mem_stats(device: Optional[Union[torch.device, str]] = None) -> Dict[str, Any]:
    _tensors = all_tensors(device)
    output = {
        "num tensors": len(_tensors),
        "total mem": sum(x.nelement() * x.element_size() for x in _tensors),
    }
    return output


@attr.s(auto_attribs=True)
class TruncateIter:
    num: int
    iterable_to_wrap: Iterable

    def __iter__(self):
        self.iter = iter(self.iterable_to_wrap)
        self.counter = 0
        return self

    def __next__(self):
        if self.counter > self.num:
            raise StopIteration
        else:
            self.counter += 1
            return next(self.iter)

    def __len__(self):
        return self.num


def unique(
    x: Tensor,
    sorted: bool = False,
    return_index: bool = False,
    return_inverse: bool = False,
    return_counts: bool = False,
) -> Tuple[Tensor, ...]:
    """
    Wrapper around PyTorch unique which supports the return_index option.
    See: https://github.com/rusty1s/pytorch_unique
    """
    if not return_index:
        return x.unique(
            sorted=sorted, return_inverse=return_inverse, return_counts=return_counts
        )
    else:
        unique_tensor, inverse, *other = x.unique(
            sorted=sorted, return_inverse=True, return_counts=return_counts
        )
        perm = torch.arange(inverse.size(0), dtype=inverse.dtype, device=inverse.device)
        inverse, perm = inverse.flip([0]), perm.flip([0])
        index = inverse.new_empty(unique_tensor.size(0)).scatter_(0, inverse, perm)
        if return_inverse:
            return (unique_tensor, index, inverse, *other)
        else:
            return (unique_tensor, index, *other)
