# Copyright 2023 The pytorch-utils Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from typing import *

from loguru import logger

__all__ = [
    "Logger",
]


class Logger:
    def __init__(self):
        self._log_dict = {}

    def collect(self, d: dict, overwrite: Optional[bool] = None) -> dict:
        """
        Collect values to log upon commit.
        :param d: dictionary of metrics to collect
        :param overwrite: whether or not to overwrite (default is to warn)
        :return: dictionary of currently connected metrics
        """
        for k, v in d.items():
            if not overwrite:
                if k in self._log_dict:
                    same_value = False
                    # We try several ways to assess equality
                    try:
                        same_value = v == self._log_dict[k]
                    except:
                        pass
                    try:
                        same_value = (v == self._log_dict[k]).all()
                    except:
                        pass
                    if not same_value:
                        if overwrite is False:
                            message = (
                                f"Logger had uncommitted value `{k}={self._log_dict[k]}`, attempted to overwrite with "
                                f"`{k}={v}` but overwrite is set to False."
                            )
                            logger.warning(message)
                            raise RuntimeError(message)
                        else:
                            logger.warning(
                                f"Logger had uncommitted value `{k}={self._log_dict[k]}`, overwriting with `{k}={v}`."
                            )
            self._log_dict[k] = v
        return self._log_dict

    def commit(self) -> None:
        """
        If there are values to log, log them.
        """
        self.clear()

    def clear(self) -> None:
        """
        Clear collected log values.
        """
        self._log_dict = {}

    @property
    def has_collected_data(self) -> bool:
        return len(self._log_dict) > 0
