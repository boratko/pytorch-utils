# Copyright 2023 The pytorch-utils Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import annotations

from pathlib import Path
from typing import *

import attr
import torch
from loguru import logger
from torch.nn import Module

from .exceptions import EarlyStoppingException

__all__ = [
    "EarlyStopping",
    "ModelCheckpoint",
    "IntervalConditional",
]


@attr.s(auto_attribs=True)
class EarlyStopping:
    """
    Stop looping if a value is stagnant.
    """

    name: str = "EarlyStopping value"
    patience: int = 10

    def __attrs_post_init__(self):
        self.count = 0
        self.value = None

    def __call__(self, value) -> None:
        if value == self.value:
            self.count += 1
            if self.count >= self.patience:
                raise EarlyStoppingException(
                    f"{self.name} has not changed in {self.patience} steps."
                )
        else:
            self.value = value
            self.count = 0


@attr.s(auto_attribs=True)
class ModelCheckpoint:
    """
    When called, saves current model parameters to system RAM.
    """

    run_dir: Path = attr.ib(default="", converter=Path)
    filename: str = "learned_model.pt"

    def __attrs_post_init__(self):
        self._best_model_state_dict = None

    @property
    def path(self):
        return self.run_dir / self.filename

    def best_model_state_dict(self):
        if self._best_model_state_dict is None:
            raise ValueError(
                "best_model_state_dict does not exist because this ModelCheckpoint object has never been called"
            )
        else:
            return self._best_model_state_dict

    def __call__(self, model: Module) -> None:
        self._best_model_state_dict = {
            k: v.detach().cpu().clone() for k, v in model.state_dict().items()
        }

    def save_to_disk(self) -> None:
        """
        Saves model which was previously saved to RAM to disk.
        """
        logger.info(f"Saving model as '{self.path}'")
        torch.save(self.best_model_state_dict(), self.path)
        logger.info("Complete!")


@attr.s(auto_attribs=True)
class IntervalConditional:
    interval: Union[int, float]
    last: Union[int, float] = 0

    def __call__(self, value: Union[int, float]) -> bool:
        """Return True when value has exceeded the previous True by at least self.interval, otherwise return False"""
        if value - self.last >= self.interval:
            self.last = value
            return True
        else:
            return False

    @classmethod
    def interval_conditional_converter(cls, value) -> Optional[IntervalConditional]:
        """Converter helper function for IntervalConditional"""
        if value is None:
            return None
        else:
            return IntervalConditional(value)
