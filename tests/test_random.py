# Copyright 2023 The pytorch-utils Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import torch
import numpy as np
from pytorch_utils.random import RandomIntsAvoid
from hypothesis import given, strategies as st
from hypothesis.extra import numpy as hnp


@st.composite
def generate_unique_ints(
    draw, max=st.integers(min_value=2, max_value=1_000_000_000),
):
    max = draw(max)
    num_ints = draw(st.integers(min_value=1, max_value=min(1_000, max - 1)))
    ints = draw(
        hnp.arrays(
            dtype=np.int64,
            shape=(num_ints,),
            elements=st.integers(min_value=0, max_value=max - 1),
            unique=True,
        )
    )
    return torch.from_numpy(ints), max


@given(
    avoid_and_max=generate_unique_ints(),
    num_samples=st.integers(min_value=1, max_value=100_000),
)
def test_sample_ints_with_avoid(avoid_and_max, num_samples):

    avoid, max = avoid_and_max
    random_ints_avoid = RandomIntsAvoid(max, avoid)
    sample = random_ints_avoid(num_samples)
    assert len(sample) == num_samples
    assert (sample[None, :] != avoid[:, None]).all()


@given(
    max=st.integers(min_value=2, max_value=10_000_000),
    num_samples=st.integers(min_value=1, max_value=100_000),
)
def test_sample_ints_without_avoid(max, num_samples):
    random_ints = RandomIntsAvoid(max)
    sample = random_ints(num_samples)
    assert len(sample) == num_samples
